package ci

import (
	"strings"

	"gopkg.in/yaml.v3"
)

const (
	headerString = "---\n"

	RuleIsDefaultBranch    = "$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH"
	RuleIsNotDefaultBranch = "$CI_COMMIT_BRANCH != $CI_DEFAULT_BRANCH"
)

// CI defines configuration for a GitLab CI file.
// See https://docs.gitlab.com/ee/ci/yaml/index.html.
type CI struct {
	Globals
	Jobs
}

// See https://docs.gitlab.com/ee/ci/yaml/index.html#global-keywords.
type Globals struct {
	Default   `yaml:"default,omitempty"`
	Include   []IncludeRule     `yaml:"include,omitempty"`
	Stages    []string          `yaml:"stages,omitempty"`
	Variables map[string]string `yaml:"variables,omitempty"`
	Workflow  `yaml:"workflow,omitempty"`
}

// Default is a map of configuration for pipeline defaults.
// See https://docs.gitlab.com/ee/ci/yaml/index.html#default.
type Default struct {
	AfterScript   []string  `yaml:"after_script,omitempty"`
	BeforeScript  []string  `yaml:"before_script,omitempty"`
	Image         string    `yaml:"image,omitempty"`
	Interruptible bool      `yaml:"interruptible,omitempty"`
	Retry         int       `yaml:"retry,omitempty"`
	Services      []Service `yaml:"services,omitempty"`
	Tags          []string  `yaml:"tags,omitempty"`
	Timeout       string    `yaml:"timeout,omitempty"`
}

// IncludeRule is a map of configuration of include rules.
type IncludeRule struct {
	Local    string `yaml:"local,omitempty"`
	Project  string `yaml:"project,omitempty"`
	Remote   string `yaml:"remote,omitempty"`
	Template string `yaml:"template,omitempty"`
}

type Workflow struct {
	Name  string         `yaml:"name,omitempty"`
	Rules []WorkflowRule `yaml:"rules,omitempty"`
}

// WorkflowRule is a map of configuration for workflow rules.
type WorkflowRule struct {
	If        string            `yaml:"if"`
	Variables map[string]string `yaml:"variables,omitempty"`
	When      string            `yaml:"when,omitempty"`
}

// Jobs is a map of Job names to JobConfigs.
type Jobs map[string]JobConfig

// JobConfig defines the job configuration keywords in GitLab Ci.
// See https://docs.gitlab.com/ee/ci/yaml/index.html#job-keywords.
type JobConfig struct {
	Stage        string         `yaml:"stage,omitempty"`
	Image        string         `yaml:"image,omitempty"`
	BeforeScript []string       `yaml:"before_script,omitempty"`
	Script       []string       `yaml:"script,omitempty"`
	AfterScript  []string       `yaml:"after_script,omitempty"`
	AllowFailure bool           `yaml:"allow_failure,omitempty"`
	Extends      string         `yaml:"extends,omitempty"`
	Needs        []string       `yaml:"needs,omitempty"`
	Retry        int            `yaml:"retry,omitempty"`
	Rules        []WorkflowRule `yaml:"rules,omitempty"`
	Services     []Service      `yaml:"services,omitempty"`
	Tags         []string       `yaml:"tags,omitempty"`
	Timeout      string         `yaml:"timeout,omitempty"`
}

// Service is a map of configuration for services.
// See https://docs.gitlab.com/ee/ci/yaml/index.html#services.
type Service struct {
	Name       string   `yaml:"name"`
	Alias      string   `yaml:"alias,omitempty"`
	Entrypoint []string `yaml:"entrypoint,omitempty"`
	Command    []string `yaml:"command,omitempty"`
}

// Build constructs the GitLab CI file in YAML format.
func (c CI) Build() (string, error) {
	var result strings.Builder
	result.WriteString(headerString)

	globals, err := yaml.Marshal(c.Globals)
	if err != nil {
		return "", err
	}

	result.Write(globals)

	jobs, err := yaml.Marshal(c.Jobs)
	if err != nil {
		return "", err
	}

	result.Write(jobs)

	return result.String(), nil
}

// String is a helper method to print out the CI
// configuration in readable YAML format.
func (c CI) String() string {
	result, err := c.Build()
	if err != nil {
		return ""
	}

	return result
}
