# gitlab-ci-go

Write GitLab CI pipelines using Golang.

## Why?

As YAML is only a markup langauge and not a full-blown
programming language, it has some inherent limitations.

This Go library allows you to write the CI configuration
with all the benefits of a programming language behind you,
meaning:

- Development environments can be used to better map, trace,
  and debug configuration.
- Easily share configuration using Go modules.
- Configuration can be DRY (without meddling with anchors).
- You aren't focused on counting spaces in YAML.
- It's easier to write tests.

This approach is similar to
[Pulumi's Cloud Native SDK](https://www.pulumi.com/docs/get-started/kubernetes/review-project/)
which supports constructing Kubernetes manifests using many different languages,
including Go.

## Known limitations

Only a subset of [GitLab CI options](https://docs.gitlab.com/ee/ci/yaml)
are exposed by this package. It's fairly straightforward to add more, but
this package will naturally lag behind all supported features that are defined
in the [GitLab project](https://gitlab.com/gitlab-org/gitlab).

Because GitLab CI is written in Ruby, this package needs to translate options
into Golang. If GitLab CI were ever written in Golang instead, we could leverage
that code and not need to redefine all of the options into Go structs.

In its current state, and given the limitations above, this package is only
helpful for projects with a fairly straightforward CI implementation since not
all configuration options are exposed here. Additionally, this is currently a
side project and therefore not officially supported. I recommend picking a tagged
release to avoid any unexpected changes.

## How to use it

This package leverages GitLab's
[Dynamic child pipelines](https://docs.gitlab.com/ee/ci/pipelines/downstream_pipelines.html#dynamic-child-pipelines)
feature, which allows you to generate a CI configuration file and use it in a
child pipeline.

First, create a CI pipeline in `.gitlab-ci.yml`:

```yaml
include:
  - project: 'mnielsen/gitlab-ci-go'
    file: '/ci/templates/.gitlab-ci.yml'
```

Next, configure CI in your project:

1. Create a top-level `ci/` directory (if it doesn't already exist).
1. Run `go mod init <GitLab URL>/<project path>/ci`.
1. Run `go get gitlab.com/mnielsen/gitlab-ci-go@<version>`.
1. Create a new file named `generate.go`.
1. Add your CI configuration in `generate.go`. For an example, see
   [this CI example](https://gitlab.com/mnielsen/1on1-report-bot/-/blob/master/ci/generate.go).

When these changes are pushed to your project, you will see CI run the `generate-config`
job. This runs `go run generate.go` from your `ci` directory, writing the output
to a new file named `generated-config.yml`. This file is saved as a CI artifact.

Next, the child pipeline will run using the `generated-config.yml` file as the
source for the pipeline configuration.

That's it!
